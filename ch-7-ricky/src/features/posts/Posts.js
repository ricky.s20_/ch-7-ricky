import React, { useState, useEffect , useMemo } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Form } from 'react-bootstrap'
import { getPosts, data } from "./reducer";
import {
  Grid,
  Select,
  Card,
  CardContent,
  Container,
  Button,
  Skeleton,
  Typography
} from "@mui/material";
import { useNavigate } from "react-router-dom";
import "./css/style2.css";
import img2 from "./img/img_car.png";
// import image from "./images/"

const Posts = () => {
  const posts = useSelector(data);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  
  const [filters, setFilter] = useState({ 
    tipe: '',
    datepick: '',
    time: '',
    user: '',
  });
  const [submitFilter, setSubmitFilter] = useState({});
  
  
  const fetchData = () => {
    dispatch(getPosts());
  };

  useEffect(() => {
    fetchData();
  }, []);
  
  const handleSubmit = (e) => {
    e.preventDefault()
    setSubmitFilter(filters)
  }

  const handleChange = (e) => {
    setFilter({ ...filters, [e.target.name]: e.target.value })
  }

  const carFilter = useMemo(() => {
   
    return posts.filter((car) => {
      const tipeDriver = submitFilter.tipe == 'true'
      const date = Date.parse(submitFilter.datepick)
      const time = submitFilter.time
      const user1 = submitFilter.user
      //ambil data dari car.example.js lalu ubah ke bentuk string
      const dateData = new Date(car.availableAt).getTime() 
      const timeData = car.availableAt.split('T')
      const timeData2 = timeData[1].split(':')

      // console.log(timeData2)
      // console.log(date)

      if (
        submitFilter.user && submitFilter.tipe && submitFilter.datepick && submitFilter.time
      ) {
        return (
         car.capacity > user1 && tipeDriver === car.available && date >= dateData && time >= timeData2[0]
        )
      } 
      return true
    })
  }, [posts, submitFilter])
 
  const handleClick = (id) => {
    navigate(`/posts/${id}`);
  };


  //  console.log(carFilter);

  return (
    <>
       <main  style={{width: '100%', height: '100%', marginBottom: '5%'}}>
          <section className="hal1" id="hal1">
              <div className="container-fluid">
                  <div className="home-section">
                      <div className="justify-center">
                          <h2 className="text-judul">Sewa & Rental Mobil Terbaik di kawasan Surabaya</h2>
                          <h6 className="text-judul-desc">Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.</h6>
                      </div>
                      <div className="justify-content-center area-gambar" style={{paddingRight: '0%',marginTop: '60px'}}>  
                          <img src={img2} className="img-fluid float-end area-mobil" style={{marginRight: '0px'}}/>
                      
                      </div>  
                  </div>
              </div>
          </section>
          <div className="card filter-search" >
      
          <Form onSubmit={handleSubmit}
            onChange={handleChange}>
            <div className="card-body-new">
              <div className="search-menu">
              <p>Type Driver</p>
              <Form.Select id="available" className="form-select" aria-label="Default select example" name="tipe">
                  <option selected value="0">Pilih Tipe Driver</option>
                  <option value="true">Dengan Sopir</option>
                  <option value="false">Tanpa Sopir (Lepas Kunci)</option>
              </Form.Select>
              </div>
              <div className="search-menu">
              <p>Tanggal</p>
              <Form.Control id="date" type="date" className="form-control" placeholder="Pilih Tanggal" name="datepick"/>
              </div>
              <div className="search-menu">
              <p>Waktu Jemput/Ambil</p>
              <Form.Select id="time" className="form-select" aria-label="Default select example" name="time">
                  <option selected value="0">Pilih Waktu</option>
                  <option value="9">09.00 AM</option>
                  <option value="10">10.00 AM</option>
                  <option value="15">15.00 AM</option>
                  <option value="17">17.00 AM</option>
              </Form.Select>
              
              </div>
              <div className="search-menu">
              <p>Jumlah Penumpang</p>
              <Form.Control id="user" type="text" className="form-control" placeholder="Jumlah Penumpang" name="user"/>
              </div>
              <div className="search-menu">
              <div className="button-nav">
                  <button type="submit" className="btn button-green lebar" id="find">Cari Mobil</button>
              </div>
              </div>

            </div>
          </Form>
          </div>
          
          <Container style={{marginTop: '50px'}}>
              <Grid container spacing={3}>
                {carFilter.map((row, i) => ( 
                  <Grid key={i} item xs={4}>
                  <div className="card-item"> 
                      <div className="foto"> 
                        <img src={row?.image} alt={row?.manufacture} width="64px"/>
                      </div>
                      <div className="desc">
                        <p style={{textAlign: 'left'}}>{row?.manufacture} / {row?.type}</p>
                        <p style={{textAlign: 'left',fontWeight: '700'}} >Rp {row?.rentPerDay} / hari</p>
                        <p style={{textAlign: 'left'}}>{row?.description}</p>
                        <p style={{textAlign: 'left'}}> {row?.capacity}Orang</p>
                        <p style={{textAlign: 'left'}}> {row?.transmission}</p>
                        <p style={{textAlign: 'left'}}>Tahun {row?.year}</p>
                        <p style={{textAlign: 'left'}}>{row?.availableAt} </p>
                      </div>
                      <Button variant="contained" color="success" onClick={() => handleClick(row?.id)}>Pilih Mobil</Button>
                  </div>
                  </Grid>
                ))}
                {posts.status === "loading" && <Typography>Loading</Typography>}
              </Grid>
            </Container>
      </main>
    </>
  );
};

export default Posts;
