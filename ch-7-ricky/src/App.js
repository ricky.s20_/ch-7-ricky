import React from 'react';
import logo from './logo.svg';
import { Counter } from './features/counter/Counter';
import './App.css';
import { Routes, Route } from 'react-router-dom'; 
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.js';
import { Navbar, Nav, Container, Button } from 'react-bootstrap';
import Landing from "./app/pages/Landing";
import List from "./features/posts/Posts";

// import { useNavigate } from 'react-router-dom';

function App() {
  const menu = [
    {path: "/", component: <Landing />},
    {path: "/posts", component: <List />}
  ]

  return (
    <div className="App">
        <Navbar bg="primary" variant="dark">
          <Container>
            <Nav className="me-auto">
              <Nav.Link href="#home">Home</Nav.Link>
            </Nav>
            <Nav className="me-auto-right">
              <Nav.Link href="#home">Our Service</Nav.Link>
              <Nav.Link href="#home">Why Us</Nav.Link>
              <Nav.Link href="#home">Testimonial</Nav.Link>
              <Nav.Link href="#home">Faq</Nav.Link>
              <Button variant="success" href="/register">Register</Button>{' '}
            </Nav>
          </Container>
        </Navbar>
        <div class="App">
          <Routes>
            {menu.map((row,i) =>
              <Route exact path= {row.path} element={row.component}></Route>
            )}
          </Routes>
        </div>
    </div>
  );
}

export default App;
