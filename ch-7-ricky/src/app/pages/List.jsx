import '../fontawesome-free-6.0.0-web/css/all.min.css'
import '../fontawesome-free-6.0.0-web/css/fontawesome.min.css'
import '../../App.css'
import Post from "../../features/posts/Posts";
import img2 from "../img/img_car.png";
import "../css/style2.css";

function App() {

    return (
        <>
            <main  style={{width: '100%', height: '100%', marginBottom: '5%'}}>
                <section className="hal1" id="hal1">
                    <div className="container-fluid">
                        <div className="home-section">
                            <div className="justify-center">
                                <h2 className="text-judul">Sewa & Rental Mobil Terbaik di kawasan Surabaya</h2>
                                <h6 className="text-judul-desc">Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.</h6>
                            </div>
                            <div className="justify-content-center area-gambar" style={{paddingRight: '0%',marginTop: '60px'}}>  
                                <img src={img2} className="img-fluid float-end area-mobil" style={{marginRight: '0px'}}/>
                            
                            </div>  
                        </div>
                    </div>
                </section>
                <div className="card filter-search" >
                <div className="card-body-new">
                    <div className="search-menu">
                    <p>Type Driver</p>
                    <select id="available" className="form-select" aria-label="Default select example">
                        <option selected value="0">Pilih Tipe Driver</option>
                        <option value="true">Dengan Sopir</option>
                        <option value="false">Tanpa Sopir (Lepas Kunci)</option>
                    </select>
                    </div>
                    <div className="search-menu">
                    <p>Tanggal</p>
                    <input id="date" type="text" onfocus="(this.type='date')" className="form-control" placeholder="Pilih Tanggal"/>
                    </div>
                    <div className="search-menu">
                    <p>Waktu Jemput/Ambil</p>
                    <select id="time" className="form-select" aria-label="Default select example">
                        <option selected value="0">Pilih Waktu</option>
                        <option value="540">09.00 AM</option>
                        <option value="600">10.00 AM</option>
                        <option value="660">11.00 AM</option>
                        <option value="720">12.00 AM</option>
                    </select>
                    
                    </div>
                    <div className="search-menu">
                    <p>Jumlah Penumpang</p>
                    <input id="user" type="text" className="form-control" placeholder="Jumlah Penumpang"/>
                    </div>
                    <div className="search-menu">
                    <div className="button-nav">
                        <button type="button" className="btn button-green lebar" id="find">Cari Mobil</button>
                    </div>
                    </div>
                </div>
                </div>
                
            </main>
        </>
    );
}

export default App;
