import Img from '../img/img_car.png'
import Imgservice from '../img/img_service.png'
import Img2 from '../img/img_photo(1).png'
import Img3 from '../img/img_photo(2).png'
import logo from '../img/logo.jpg'
import '../fontawesome-free-6.0.0-web/css/all.min.css'
import '../fontawesome-free-6.0.0-web/css/fontawesome.min.css'
import '../css/style.css'
import '../../App.css'

function App() {

    return (
        <>
            <section className="hal1" id={"hal1"}>
                <div className="container-fluid">
                    <div className="home-section">
                            <div className="justify-center">
                                <h2 className="text-judul">Sewa & Rental Mobil Terbaik di kawasan Surabaya</h2>
                                <h6 className="text-judul-desc">Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.</h6>
                                <a href="/posts"><button type="button" className="btn button-green">Mulai Sewa Mobil</button></a>
                            </div>
                            <div className="justify-content-center area-gambar" style={{paddingRight: '0%',marginTop: '60px'}}>  
                                <img src={Img} className="img-fluid float-end area-mobil" style={{marginRight: '0px', right: '100px'}} />
                            </div>  
                    </div>
                </div>
            </section>
            <section className="hal2" id={"hal2"}>
                <div className="container">
                    <div className="row home-section-new">
                        <div className="justify-center">
                            <img src={Imgservice} width="90%" height="90%" />
                        </div>  
                        <div className="justify-center" >
                            <h2 className="text-h2-2">Best Car Rental for any kind of trip in Surabaya!</h2>
                            <h6 className="text-h6-2">Sewa mobil di Surabaya bersama Binar Car Rental jaminan harga lebih murah dibandingkan yang lain, kondisi mobil baru, serta kualitas pelayanan terbaik untuk perjalanan wisata, bisnis, wedding, meeting, dll.</h6>
                            <table>
                                <tbody>
                                    <tr><th style={{height: '50px'}}><i className="fa-solid fa-check circle-2" style={{alignItems: 'center', display: 'flex'}}></i></th><td>Sewa Mobil Dengan Supir di Bali 12 Jam</td></tr>
                                    <tr><th style={{height: '50px'}}><i className="fa-solid fa-check circle-2" style={{alignItems: 'center', display: 'flex'}}></i></th><td>Sewa Mobil Lepas Kunci di Bali 24 Jam</td></tr>
                                    <tr><th style={{height: '50px'}}><i className="fa-solid fa-check circle-2" style={{alignItems: 'center', display: 'flex'}}></i></th><td>Sewa Mobil Jangka Panjang Bulanan</td></tr>
                                    <tr><th style={{height: '50px'}}><i className="fa-solid fa-check circle-2" style={{alignItems: 'center', display: 'flex'}}></i></th><td>Gratis Antar - Jemput Mobil di Bandara</td></tr>
                                    <tr><th style={{height: '50px'}}><i className="fa-solid fa-check circle-2" style={{alignItems: 'center', display: 'flex'}}></i></th><td>Layanan Airport Transfer / Drop In Out</td></tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
            <section className="hal3" id="hal3">
                <div className="container">
                    <h1 className="text-h1-3">Why Us?</h1>
                    <h5 className="text-h5-3" style={{marginBottom: '5%'}}>Mengapa harus pilih Binar Car Rental?</h5>
                    <div className="home-section-3">
                        <div className="card">
                            <div className="card-body">
                                <i className="fa-solid fa-thumbs-up circle-yellow"  style={{display: 'flex', alignItems:'center', color: 'transparent', '-webkit-text-stroke-width': '1px', '-webkit-text-stroke-color': 'rgb(255, 255, 255)'}}></i>
                                
                                <h2 className="card-text text-h2-3">Mobil Lengkap</h2>
                                <p className="card-text text-p-3">Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat</p>
                            </div> 
                        </div>
                        <div className="card">
                            <div className="card-body">
                                <i className="fa-solid fa-tag circle-red"  style={{display: 'flex', alignItems:'center', color: 'transparent', '-webkit-text-stroke-width': '1px', '-webkit-text-stroke-color': 'rgb(255, 255, 255)'}}></i>
                               
                                <h2 className="card-text text-h2-3">Harga Murah</h2>
                                <p className="card-text text-p-3">Harga murah dan bersaing, bisa bandingkan harga kami dengan rental mobil lain</p>
                            </div> 
                        </div>
                        <div className="card">
                            <div className="card-body">
                                <i className="fa-solid fa-clock circle-blue" style={{display: 'flex', alignItems:'center', color: 'transparent', '-webkit-text-stroke-width': '1px', '-webkit-text-stroke-color': 'rgb(255, 255, 255)'}}></i>
                                <h2 className="card-text text-h2-3">Layanan 24 Jam</h2>
                                <p className="card-text text-p-3">Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga tersedia di akhir minggu</p>
                            </div> 
                        </div>               
                        <div className="card">
                            <div className="card-body">
                                <i className="fa-solid fa-award circle-green"  style={{display: 'flex', alignItems:'center', color: 'transparent', '-webkit-text-stroke-width': '1px', '-webkit-text-stroke-color': 'rgb(255, 255, 255)'}}></i>
                                <h2 className="card-text text-h2-3">Sopir Profesional</h2>
                                <p className="card-text text-p-3">Sopir yang profesional, berpengalaman, jujur, ramah dan selalu tepat waktu</p>
                            </div> 
                        </div>
                    </div>
                </div>
            </section>
            <section className="hal4" id="hal4">
                <div className="container">
                    <h1 style={{textAlign:'center', fontFamily: 'Helvetica', fontStyle: 'normal'}}>Testimonial</h1>
                    <h6 className="mb-5"style={{textAlign:'center', fontFamily: 'Helvetica', fontStyle: 'normal'}}>Berbagai review positif dari pelanggan kami</h6>
                    <div id="testimonial"></div>
                    <div id="carouselExampleControls" className="carousel slide" data-bs-ride="carousel">
                        <div className="carousel-inner">
                        <div className="carousel-item active" id="slide-pertama">
                            <div className="card" style={{paddingLeft: '3%',  paddingRight: '3%'}}>
                                <div className="card-grid" >
                                    <div className="img-4">
                                    <img src={Img2}/>
                                    </div>
                                    <div className="desc" >
                                    <div className="rating">
                                        <i className="fa-solid fa-star" style={{color: 'yellow'}}></i>
                                        <i className="fa-solid fa-star" style={{color: 'yellow'}}></i>
                                        <i className="fa-solid fa-star" style={{color: 'yellow'}}></i>
                                        <i className="fa-solid fa-star" style={{color: 'yellow'}}></i>
                                        <i className="fa-solid fa-star" style={{color: 'yellow'}}></i>
                                    </div>
                                    <div className="pa-4" >
                                        <p>
                                        “Lorem ipsum dolor sit amet, consectetur adipiscing
                                        elit, sed do eiusmod lorem ipsum dolor sit amet,
                                        consectetur adipiscing elit, sed do eiusmod lorem ipsum
                                        dolor sit amet, consectetur adipiscing elit, sed do
                                        eiusmod”
                                        </p>
                                        <p className="p-4-1">John Dee 32, Bromo</p>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="carousel-item" id="slide-kedua">
                                <div className="card" style={{paddingLeft: '3%', paddingRight: '3%'}}>
                                    <div className="card-grid" >
                                    <div className="img-4">
                                        <img src={Img2}/>
                                    
                                    </div>
                                    <div className="desc" >
                                        <div className="rating">
                                            <i className="fa-solid fa-star" style={{color: 'yellow'}}></i>
                                            <i className="fa-solid fa-star" style={{color: 'yellow'}}></i>
                                            <i className="fa-solid fa-star" style={{color: 'yellow'}}></i>
                                            <i className="fa-solid fa-star" style={{color: 'yellow'}}></i>
                                            <i className="fa-solid fa-star" style={{color: 'yellow'}}></i>
                                        </div>
                                        <div className="pa-4" >
                                        <p>
                                            “Lorem ipsum dolor sit amet, consectetur adipiscing
                                            elit, sed do eiusmod lorem ipsum dolor sit amet,
                                            consectetur adipiscing elit, sed do eiusmod lorem ipsum
                                            dolor sit amet, consectetur adipiscing elit, sed do
                                            eiusmod”
                                        </p>
                                        <p className="p-4-1">John Dee 32, Bromo</p>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                        </div>
                        <div className="carousel-item" id="slide-ketiga">
                                <div className="card" style={{paddingLeft: '3%', paddingRight: '3%'}}>
                                    <div className="card-grid" >
                                    <div className="img-4">
                                        <img src={Img3}/>
                                    </div>
                                    <div className="desc" >
                                        <div className="rating">
                                            <i className="fa-solid fa-star" style={{color: 'yellow'}}></i>
                                            <i className="fa-solid fa-star" style={{color: 'yellow'}}></i>
                                            <i className="fa-solid fa-star" style={{color: 'yellow'}}></i>
                                            <i className="fa-solid fa-star" style={{color: 'yellow'}}></i>
                                            <i className="fa-solid fa-star" style={{color: 'yellow'}}></i>
                                        </div>
                                        <div className="pa-4" >
                                        <p>
                                            “Lorem ipsum dolor sit amet, consectetur adipiscing
                                            elit, sed do eiusmod lorem ipsum dolor sit amet,
                                            consectetur adipiscing elit, sed do eiusmod lorem ipsum
                                            dolor sit amet, consectetur adipiscing elit, sed do
                                            eiusmod”
                                        </p>
                                        <p className="p-4-1">John Dee 32, Bromo</p>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                        </div>
                        </div>
                        <div className="button-4" style={{marginTop: '5%'}}>
                            <button type="button" className="btn btn-outline-success btn-circle btn-xl">
                                <i className="fa-solid fa-chevron-left" data-bs-slide="prev" data-bs-target="#carouselExampleControls"></i>
                            </button>
                            <button type="button" className="btn btn-outline-success btn-circle btn-xl">
                                <i className="fa-solid fa-chevron-right" data-bs-slide="next" data-bs-target="#carouselExampleControls"></i>
                            </button>          
                        </div>   
                    </div>     
                </div>
            </section>
            <section className="hal5" id="hal5">
                <div className="container">
                        <div className="card-5">
                            <h1>Sewa Mobil di Surabaya Sekarang</h1>
                            <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                            eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            </p>
                            <button type="button" className="btn btn-success">Mulai Sewa Mobil</button>
                        </div>
                </div>
            </section>
            <section className="hal6" id="hal6">
                <div className="container">
                    <div className="row grid-6">
                        <div className="text-6">
                        <h2 className="h2-6">Frequently Asked Question</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                        </div>
                        <div className="acc-6">
                        <div className="accordion" id="accordionExample">
                            <div className="mb-3">
                            <div className="accordion-item">
                                <h2 className="accordion-header" id="headingOne">
                                <button className="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Apa saja syarat yang dibutuhkan?
                                </button>
                                </h2>
                                <div id="collapseOne" className="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                <div className="accordion-body">
                                    Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                                    Delectus debitis ex, expedita accusantium impedit ipsum at,
                                    quasi error laborum voluptas dolore possimus totam! Illo
                                    sapiente et quos sint, saepe totam.
                                </div>
                                </div>
                            </div>
                            </div>
                            <div className="mb-3">
                            <div className="accordion-item">
                                <h2 className="accordion-header" id="headingTwo">
                                <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Berapa hari minimal sewa mobil lepas kunci?
                                </button>
                                </h2>
                                <div id="collapseTwo" className="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                                <div className="accordion-body">
                                    Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                                    Delectus debitis ex, expedita accusantium impedit ipsum at,
                                    quasi error laborum voluptas dolore possimus totam! Illo
                                    sapiente et quos sint, saepe totam.  
                                </div>
                                </div>
                            </div>
                            </div>
                            <div className="mb-3">
                            <div className="accordion-item">
                                <h2 className="accordion-header" id="headingThree">
                                <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    Berapa hari sebelumnya sebaiknya booking sewa mobil?
                                </button>
                                </h2>
                                <div id="collapseThree" className="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                                <div className="accordion-body">
                                    Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                                    Delectus debitis ex, expedita accusantium impedit ipsum at,
                                    quasi error laborum voluptas dolore possimus totam! Illo
                                    sapiente et quos sint, saepe totam.  
                                </div>
                                </div>
                            </div>
                            </div>
                            <div className="mb-3">
                            <div className="accordion-item">
                                <h2 className="accordion-header" id="headingFour">
                                <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    Apakah Ada biaya antar-jemput?
                                </button>
                                </h2>
                                <div id="collapseFour" className="accordion-collapse collapse" aria-labelledby="headingFour" data-bs-parent="#accordionExample">
                                <div className="accordion-body">
                                    Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                                    Delectus debitis ex, expedita accusantium impedit ipsum at,
                                    quasi error laborum voluptas dolore possimus totam! Illo
                                    sapiente et quos sint, saepe totam.  
                                </div>
                                </div>
                            </div>
                            </div>
                            <div className="mb-5">
                            <div className="accordion-item">
                                <h2 className="accordion-header" id="headingFive">
                                <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    Bagaimana jika terjadi kecelakaan
                                </button>
                                </h2>
                                <div id="collapseFive" className="accordion-collapse collapse" aria-labelledby="headingFive" data-bs-parent="#accordionExample">
                                <div className="accordion-body">
                                    Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                                    Delectus debitis ex, expedita accusantium impedit ipsum at,
                                    quasi error laborum voluptas dolore possimus totam! Illo
                                    sapiente et quos sint, saepe totam.  
                                </div>
                                </div>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </section>
            <footer>
                <div className="container">
                    <div className="footer">
                        <div className="text-f-1">
                        <li>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</li>
                        <li>binarcarrental@gmail.com</li>
                        <li>081-233-334-808</li>
                        </div>
                        <div className="text-f-2">
                        <a href="#hal2"><li>Our services</li></a>
                        <a href="#hal3"><li>Why Us</li></a>
                        <a href="#hal4"><li>Testimonial</li></a>
                        <a href="#hal6"><li>FAQ</li></a>
                        </div>
                        <div className="text-f-3" >
                        <p>Connect with us</p>
                        <div className="logo">
                            <a href="https://www.facebook.com/ricky.supriyanto.902/">
                            <i className="fa-brands fa-facebook-f circle" style={{ display: 'flex', alignItems:'center' ,color: 'transparent', '-webkit-text-stroke-width': '1px', '-webkit-text-stroke-color': 'white'}}></i>
                            </a>
                            <a href="https://www.instagram.com/ricky.s20_/">
                            <i className="fa-brands fa-instagram circle ml-2" style={{ display: 'flex', alignItems:'center' ,color: 'transparent', '-webkit-text-stroke-width': '1px', '-webkit-text-stroke-color': 'white'}}>
                            </i>
                            </a>
                            <a href="https://twitter.com/RickySupriyan10?t=q1ZCCsGBoCAzf5g_5Y2UWQ&s=08">
                            <i className="fa-brands fa-twitter circle ml-2" style={{ display: 'flex', alignItems:'center' ,color: 'transparent', '-webkit-text-stroke-width': '1px', '-webkit-text-stroke-color': 'white'}}>
                            </i>
                            </a>
                            <a href="rickysupriyanto807@gmail.com">
                            <i className="fa-solid fa-envelope circle ml-2" style={{ display: 'flex', alignItems:'center' ,color: 'transparent', '-webkit-text-stroke-width': '1px', '-webkit-text-stroke-color': 'white'}}>
                            </i>
                            </a>
                            <a href="https://dashboard.twitch.tv/u/ricky_20s">
                            <i className="fa-brands fa-twitch circle ml-2" style={{ display: 'flex', alignItems:'center' ,color: 'transparent', '-webkit-text-stroke-width': '1px', '-webkit-text-stroke-color': 'white'}}>
                            </i>
                            </a>
                        </div>
                        
                        </div>
                        <div className="text-f-4">
                        <p>Copyright Binar 2022</p>
                        <div >
                            <img src={logo} width="100px" height="50px"/>
                        </div>
                        </div>
                    </div>
                </div>
                </footer>

            <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>

        </>
    );
}

export default App;
